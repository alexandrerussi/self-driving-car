#include <ros.h>
#include <std_msgs/Int16.h>

ros::NodeHandle  nh;

void velocityCallback(const std_msgs::Int16& vel){
  if(vel.data==100){
    digitalWrite(13, HIGH);
  }
  else{
    digitalWrite(13, LOW);
  }
}

ros::Subscriber<std_msgs::Int16> vel("car_vel_fuzzy", &velocityCallback);

void setup() {
  pinMode(13, OUTPUT);
  nh.initNode();
  nh.subscribe(vel);
}

void loop() {
  nh.spinOnce();
  delay(10);
}
