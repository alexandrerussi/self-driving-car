#! C:/opt/ros/noetic/x64/python.exe
# coding: utf-8

import rospy
from std_msgs.msg import Int16

import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import os

# Fetch the service account key JSON file contents
cur_dir = os.getcwd()
cred = credentials.Certificate(cur_dir + '\\deloreanapp-firebase.json')

# Initialize the app with a service account, granting admin privileges
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://deloreanapp.firebaseio.com/'
})
status_ultra_db = db.reference('/Passivo/car_status_ultrasonic')

rospy.init_node("subscriber_firebase")


def ultrasonic_callback(msg):
    # "Store" message received.
    ultrasonic = msg.data
    status_ultra_db.set(ultrasonic)


rospy.Subscriber('/car_status_ultrasonic', Int16, ultrasonic_callback)

rospy.spin()
