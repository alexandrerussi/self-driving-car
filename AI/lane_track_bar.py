import cv2
import numpy as np
import math
import sys
import time


# import RPi.GPIO as GPIO
#
# GPIO.setmode(GPIO.BCM)
# GPIO.setwarnings(False)
#
# # Throttle
# throttlePin = 13  # Physical pin 13 --> enable
# in3 = 6  # physical Pin 16
# in4 = 5  # physical Pin 18
#
# # Steering
# steeringPin = 22  # Physical Pin 22 --> enable
# in1 = 27  # Physical Pin 27
# in2 = 17  # Physical Pin 17
#
# GPIO.setup(in1, GPIO.OUT)
# GPIO.setup(in2, GPIO.OUT)
# GPIO.setup(in3, GPIO.OUT)
# GPIO.setup(in4, GPIO.OUT)
#
# GPIO.setup(throttlePin, GPIO.OUT)
# GPIO.setup(steeringPin, GPIO.OUT)
#
# # Steering
# # in1 = 1 and in2 = 0 -> Left
# GPIO.output(in1, GPIO.LOW)
# GPIO.output(in2, GPIO.LOW)
# steering = GPIO.PWM(steeringPin, 2000)
# steering.stop()
#
# # Throttle
# # in3 = 1 and in4 = 0 -> Forward
# GPIO.output(in3, GPIO.HIGH)
# GPIO.output(in4, GPIO.LOW)
# throttle = GPIO.PWM(throttlePin, 2000)
# throttle.stop()

def empty(a):
    pass


# criando nova janela
cv2.namedWindow("TrackBars")
cv2.resizeWindow("TrackBars", 640, 480)
# cv2.createTrackbar("Hue min", "TrackBars", 28, 179, empty)  # Hue min-> tonalidade minima
# cv2.createTrackbar("Hue max", "TrackBars", 179, 179, empty)
# cv2.createTrackbar("Sat min", "TrackBars", 35, 255, empty)
# cv2.createTrackbar("Sat max", "TrackBars", 240, 255, empty)
# cv2.createTrackbar("Val min", "TrackBars", 198, 255, empty)
# cv2.createTrackbar("Val max", "TrackBars", 255, 255, empty)

cv2.createTrackbar("Hue2 min", "TrackBars", 80, 179, empty)  # Hue min-> tonalidade minima
cv2.createTrackbar("Hue2 max", "TrackBars", 179, 179, empty)
cv2.createTrackbar("Sat2 min", "TrackBars", 140, 255, empty)
cv2.createTrackbar("Sat2 max", "TrackBars", 255, 255, empty)
cv2.createTrackbar("Lig min", "TrackBars", 0, 255, empty)
cv2.createTrackbar("Lig max", "TrackBars", 89, 255, empty)

cv2.createTrackbar("Canny min", "TrackBars", 100, 255, empty)
cv2.createTrackbar("Canny max", "TrackBars", 255, 255, empty)


def detect_edges(frame):
    # filter for white lane lines
    # hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)  # differentiate between colors by their level of luminance
    #
    # h_min = cv2.getTrackbarPos("Hue min", "TrackBars")
    # h_max = cv2.getTrackbarPos("Hue max", "TrackBars")
    # s_min = cv2.getTrackbarPos("Sat min", "TrackBars")
    # s_max = cv2.getTrackbarPos("Sat max", "TrackBars")
    # v_min = cv2.getTrackbarPos("Val min", "TrackBars")
    # v_max = cv2.getTrackbarPos("Val max", "TrackBars")
    # lower_white = np.array([h_min, s_min, v_min], dtype="uint8")
    # upper_white = np.array([h_max, s_max, v_max], dtype="uint8")
    # white_mask_hsv = cv2.inRange(hsv, lower_white, upper_white)

    # converting camera image to hsl
    hsl = cv2.cvtColor(frame, cv2.COLOR_BGR2HLS)

    h2_min = cv2.getTrackbarPos("Hue2 min", "TrackBars")
    h2_max = cv2.getTrackbarPos("Hue2 max", "TrackBars")
    s2_min = cv2.getTrackbarPos("Sat2 min", "TrackBars")
    s2_max = cv2.getTrackbarPos("Sat2 max", "TrackBars")
    l_min = cv2.getTrackbarPos("Lig min", "TrackBars")
    l_max = cv2.getTrackbarPos("Lig max", "TrackBars")
    # white color mask
    lower = np.uint8([h2_min, s2_min, l_min])
    upper = np.uint8([h2_max, s2_max, l_max])
    white_mask = cv2.inRange(hsl, lower, upper)

    # combine the mask to perform better in different luminosities
    # mask = cv2.bitwise_or(white_mask, white_mask_hsv)
    cv2.imshow("mask", white_mask)

    # detect edges
    can_min = cv2.getTrackbarPos("Lig min", "TrackBars")
    can_max = cv2.getTrackbarPos("Lig max", "TrackBars")
    edges = cv2.Canny(white_mask, 100, 255)
    cv2.imshow("edge", edges)

    return edges


def region_of_interest(edges):
    height, width = edges.shape  # extract the height and width of the edges frame
    mask = np.zeros_like(edges)  # make an empty matrix with same dimensions of the edges frame

    # only focus lower half of the screen
    polygon = np.array([[
        (0, height),
        (0, height / 1.5),
        (width, height / 1.5),
        (width, height),
    ]], np.int32)

    cv2.fillPoly(mask, polygon, 255)

    cropped_edges = cv2.bitwise_and(edges, mask)
    cv2.imshow("cropped_edges", cropped_edges)

    return cropped_edges


def detect_line_segments(cropped_edges):
    rho = 1
    theta = np.pi / 180
    min_threshold = 10

    # Hough is technique to detect any shape in mathematical form
    # rho: It is the the distance precision in pixels
    # theta: angular precision in radians
    # min_threshold: minimum vote it should get for it to be considered as a line
    # minLineLength: minimum length of line in pixels. Any line shorter than this number is not considered a line.
    # maxLineGap: maximum gap in pixels between 2 lines to be treated as 1 line.
    line_segments = cv2.HoughLinesP(
        image=cropped_edges,
        rho=rho,
        theta=theta,
        threshold=min_threshold,
        lines=np.array([]),
        minLineLength=5,
        maxLineGap=0
    )
    return line_segments


def average_slope_intercept(frame, line_segments):
    lane_lines = []

    # Equation of line is given by y = mx + b
    # Onde m é a inclinação da reta e b o ponto que intercepta o eixo y
    # slopes = inclinação
    # the average of slopes and intercepts of line segments detected using Hough

    if line_segments is None:
        # print("no line segments detected")
        return lane_lines
    height, width, _ = frame.shape
    left_fit = []
    right_fit = []
    boundary = 1 / 3
    left_region_boundary = width * (1 - boundary)
    right_region_boundary = width * boundary

    for line_segment in line_segments:
        for x1, y1, x2, y2 in line_segment:
            if x1 == x2:
                # print("skipping vertical lines (slope = infinity")
                continue

            fit = np.polyfit((x1, x2), (y1, y2), 1)
            slope = (y2 - y1) / (x2 - x1)
            intercept = y1 - (slope * x1)

            if slope < 0:  # left lane points
                if x1 < left_region_boundary and x2 < left_region_boundary:
                    left_fit.append((slope, intercept))
            else:  # right lane points
                if x1 > right_region_boundary and x2 > right_region_boundary:
                    right_fit.append((slope, intercept))
    left_fit_average = np.average(left_fit, axis=0)
    if len(left_fit) > 0:
        lane_lines.append(make_points(frame, left_fit_average))
    right_fit_average = np.average(right_fit, axis=0)
    if len(right_fit) > 0:
        lane_lines.append(make_points(frame, right_fit_average))

    # lane_lines is a 2D array consisting the coordinates of the right and left lane lines
    # for example: lane_lines = [[x1,y1,x2,y2],[x1,y1,x2,y2]]
    # where the left array is for left lane and the right array is for right lane
    # all coordinate points are in pixels

    return lane_lines


def make_points(frame, line):
    height, width, _ = frame.shape

    slope, intercept = line

    y1 = height  # bottom of the frame
    y2 = int(y1 / 2)  # make points from middle of the frame down

    if slope == 0:  # means that means y1 = y2 (horizontal line)
        slope = 0.1

    x1 = int((y1 - intercept) / slope)
    x2 = int((y2 - intercept) / slope)

    # return the bounded coordinates of the lane lines (from the bottom to the middle of the frame)
    return [[x1, y1, x2, y2]]


def display_lines(frame, lines, line_color=(0, 255, 0), line_width=6):
    line_image = np.zeros_like(frame)

    if lines is not None:
        for line in lines:
            for x1, y1, x2, y2 in line:
                cv2.line(line_image, (x1, y1), (x2, y2), line_color, line_width)

    line_image = cv2.addWeighted(frame, 0.8, line_image, 1, 1)  # combine two images

    return line_image


def display_heading_line(frame, steering_angle, line_color=(0, 0, 255), line_width=5):
    heading_image = np.zeros_like(frame)
    height, width, _ = frame.shape

    steering_angle_radian = steering_angle / 180.0 * math.pi

    x1 = int(width / 2)
    y1 = height
    x2 = int(x1 - height / 2 / math.tan(steering_angle_radian))
    y2 = int(height / 2)

    cv2.line(heading_image, (x1, y1), (x2, y2), line_color, line_width)
    heading_image = cv2.addWeighted(frame, 0.8, heading_image, 1, 1)

    return heading_image


def get_steering_angle(frame, lane_lines):
    height, width, _ = frame.shape

    if len(lane_lines) == 2:
        _, _, left_x2, _ = lane_lines[0][0]
        _, _, right_x2, _ = lane_lines[1][0]
        mid = int(width / 2)
        x_offset = (left_x2 + right_x2) / 2 - mid  # how much differs from the middle of the screen
        y_offset = int(height / 2)

    elif len(lane_lines) == 1:
        x1, _, x2, _ = lane_lines[0][0]
        x_offset = x2 - x1
        y_offset = int(height / 2)

    elif len(lane_lines) == 0:
        x_offset = 0
        y_offset = int(height / 2)

    angle_to_mid_radian = math.atan(x_offset / y_offset)
    angle_to_mid_deg = int(angle_to_mid_radian * 180.0 / math.pi)
    steering_angle = angle_to_mid_deg + 90

    return steering_angle


# video = cv2.VideoCapture('http://192.168.0.15:8080/video')
video = cv2.VideoCapture(1)  # open camera
# lowering the resolution to get better frame rate (fps)
video.set(cv2.CAP_PROP_FRAME_WIDTH, 320)
video.set(cv2.CAP_PROP_FRAME_HEIGHT, 240)
time.sleep(1)

speed = 8
lastTime = 0
lastError = 0
kp = 0.4
kd = kp * 0.65

while True:
    ret, frame = video.read()
    # frame = cv2.flip(frame, -1)  # flip de image vertically

    # cv2.imshow("original", frame)
    edges = detect_edges(frame)
    roi = region_of_interest(edges)
    line_segments = detect_line_segments(roi)
    lane_lines = average_slope_intercept(frame, line_segments)
    lane_lines_image = display_lines(frame, lane_lines)
    steering_angle = get_steering_angle(frame, lane_lines)
    heading_image = display_heading_line(lane_lines_image, steering_angle)
    cv2.imshow("heading line", heading_image)

    now = time.time()
    dt = now - lastTime

    deviation = steering_angle - 90
    print("angle", steering_angle)
    print("deviation", deviation)
    error = abs(deviation)

    if 5 > deviation > -5:
        deviation = 0
        error = 0
        # GPIO.output(in1, GPIO.LOW)
        # GPIO.output(in2, GPIO.LOW)
        # steering.stop()
        print("Em frente")

    elif deviation > 55:
        print("Vire a esquerda")

    elif deviation > 5:
        # GPIO.output(in1, GPIO.LOW)
        # GPIO.output(in2, GPIO.HIGH)
        # steering.start(100)
        print("Vire a direita")

    elif deviation < -55:
        print("Vire a direita")

    elif deviation < -5:
        # GPIO.output(in1, GPIO.HIGH)
        # GPIO.output(in2, GPIO.LOW)
        # steering.start(100)
        print("Vire a esquerda")

    derivative = kd * (error - lastError) / dt
    proportional = kp * error
    PD = int(speed + derivative + proportional)
    spd = abs(PD)

    # print("PID:", spd)

    if spd > 25:
        spd = 25

    # throttle.start(100)

    lastError = error
    lastTime = time.time()

    key = cv2.waitKey(1)  # wait 1ms for any keyboard button to be pressed and return ASCI code
    if key == 27:  # 27 --> ASCII code to ESC button
        break

video.release()
cv2.destroyAllWindows()
# GPIO.output(in1, GPIO.LOW)
# GPIO.output(in2, GPIO.LOW)
# GPIO.output(in3, GPIO.LOW)
# GPIO.output(in4, GPIO.LOW)
# throttle.stop()
# steering.stop()
print("FIM")
