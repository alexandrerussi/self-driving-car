#! C:/opt/ros/noetic/x64/python.exe
# coding: utf-8

import rospy
from std_msgs.msg import Int16

import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import os

# Fetch the service account key JSON file contents
cur_dir = os.getcwd()
cred = credentials.Certificate(cur_dir + '\\deloreanapp-firebase.json')

# Initialize the app with a service account, granting admin privileges
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://deloreanapp.firebaseio.com/'
})
status_object_db = db.reference('/Passivo/car_status_object')

rospy.init_node("subscriber_object")

conta = 0


def object_callback(msg):
    # "Store" message received.
    global conta
    status_object = msg.data
    if status_object == 0 and conta != 0:
        status_object_db.set("0")
        conta = 0
    elif status_object == 1 and conta != 1:
        status_object_db.set("1")
        conta = 1
    elif status_object == 2 and conta != 2:
        status_object_db.set("2")
        conta = 2
    elif status_object == 3 and conta != 3:
        status_object_db.set("3")
        conta = 3
    elif status_object == 4 and conta != 4:
        status_object_db.set("4")
        conta = 4


rospy.Subscriber('/car_status_object', Int16, object_callback)

rospy.spin()
