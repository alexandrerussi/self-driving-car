import cv2
from tkinter import *
import time
from tkinter import messagebox
import sys
import os
# initialize the list of reference points and boolean indicating
# whether cropping is being performed or not

stop_cascade = cv2.CascadeClassifier('PATH XML FILE')

global precision
cap = cv2.VideoCapture(0)

def classify():
    while True:
        ret, img = cap.read() 
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        stops = stop_cascade.detectMultiScale(gray)
        eyes = eye_cascade.detectMultiScale(gray,1.3,5)
        faces = face_cascade.detectMultiScale(gray,1.3,5)
        
        a,b,c,d,x,y,w,h,m,n,o,p = 0,0,0,0,0,0,0,0,0,0,0,0       
        
        if (a,b,c,d == 0):

            print("FACE NOT DETECTED")
            
        else :
            img = cv2.rectangle(img,(0,0),(1,1),(0,0,0),-1)
        
        for (m,n,o,p) in stops:
            img = cv2.rectangle(img,(m,n),(m+o,n+p),(0,255,0),2)


cv2.destroyAllWindows()