#! C:/opt/ros/noetic/x64/python.exe
# coding: utf-8

import rospy
from std_msgs.msg import Int16
from std_msgs.msg import UInt16
from std_msgs.msg import String

rospy.init_node("arduino_motor")
rate = rospy.Rate(100)  # Hz


import serial
ser2 = serial.Serial('COM3', 9600)

def callback_fuzzy(msg):
    vel = msg.data
    ser2.write(vel)

rospy.Subscriber('/car_vel_fuzzy', Int16, callback_fuzzy)
rospy.spin()