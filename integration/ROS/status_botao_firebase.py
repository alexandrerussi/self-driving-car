#! C:/opt/ros/noetic/x64/python.exe
# coding: utf-8

import rospy
from std_msgs.msg import Int16

import pyttsx3
import os
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

rospy.init_node("app_botao_publisher")

pub = rospy.Publisher("/car_status_botao", Int16, queue_size=10)

rate = rospy.Rate(100)  # Hz

# Fetch the service account key JSON file contents
cur_dir = os.getcwd()
cred = credentials.Certificate(cur_dir + '\\deloreanapp-firebase.json')

# Initialize the app with a service account, granting admin privileges
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://deloreanapp.firebaseio.com/'
})

# nome = db.reference('/profiles/funcionario1/nome')
status = db.reference('/Comandos/id')

engine = pyttsx3.init()  # inicializa a engine
voices = engine.getProperty('voices')

# engine.setProperty('voice', voices[0].id)  # escolhe a voz a ser falada
engine.setProperty('voice', 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Speech\Voices\Tokens\TTS_MS_EN-US_ZIRA_11.0')


def speak(text):
    engine.say(text)  # fala a string setada
    engine.runAndWait()  # executa e espera


# fala incial de boas vindas
# speak('Olá, ' + str(nome.get()) + '! Meu nome é Alfa!')
# speak('Hello, ' + str(nome.get()) + '! My name is, DeLorean!')

# Lista vozes disponiveis
# for voice in voices:
#     if voice.name == 'brazil':
#         engine.setProperty('voice', voice.id)
#     print(voice)

# status botoes
# Botão parar carro -> envia 0
# Botão carro anda livre -> envia 1
# Botão parada A -> envia 2
# Botão parada B -> envia 3

def publish(status_btn):
    status_botao = Int16()
    status_botao.data = status_btn
    pub.publish(status_botao)
    rate.sleep()


registro = 0
while not rospy.is_shutdown():
    if registro != 0 and str(status.get()) == '0':
        registro = 0
        publish(registro)
        speak('DeLorean has stopped!')
    elif registro != 1 and str(status.get()) == '1':
        speak('DeLorean will move!')
        registro = 1
        publish(registro)
    elif registro != 2 and str(status.get()) == '2':
        speak('Next station: warehouse, A!')
        registro = 2
        publish(registro)
    elif registro != 3 and str(status.get()) == '3':
        speak('Next station: warehouse, B!')
        registro = 3
        publish(registro)
    else:
        publish(registro)
