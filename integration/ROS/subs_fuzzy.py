#! C:/opt/ros/noetic/x64/python.exe
# coding: utf-8

import rospy
from std_msgs.msg import Int16

import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import os

# Fetch the service account key JSON file contents
cur_dir = os.getcwd()
cred = credentials.Certificate(cur_dir + '\\deloreanapp-firebase.json')

# Initialize the app with a service account, granting admin privileges
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://deloreanapp.firebaseio.com/'
})
vel_fuzzy_db = db.reference('/Passivo/car_vel_fuzzy')

rospy.init_node("subscriber_fuzzy")

conta = 0


def fuzzy_callback(msg):
    # "Store" message received.
    global conta
    status_object = msg.data
    if status_object == 0 and conta != 0:
        vel_fuzzy_db.set("0")
        conta = 0
    elif status_object == 128 and conta != 1:
        vel_fuzzy_db.set("128")
        conta = 1
    elif status_object == 255 and conta != 2:
        vel_fuzzy_db.set("255")
        conta = 2


rospy.Subscriber('/car_vel_fuzzy', Int16, fuzzy_callback)

rospy.spin()
