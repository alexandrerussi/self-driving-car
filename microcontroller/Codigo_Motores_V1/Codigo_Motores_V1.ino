#include <ros.h>
#include <std_msgs/Int16.h>

ros::NodeHandle nh;
int motor_anda = 8;
int motor_anda2  = 5;
int pwm_anda = 4;
int motor_vira = 7;
int motor_vira2 = 6;
int bt_lig = 12;
int bt_desl = 11;
int led_ligado = 53;

// LEDs velocidade
int led_vel_0 = 3;
//int led_vel_50 = 4;
//int led_vel_100 = 5;

// LEDs virada
int led_esquerda = 50;
//int led_frente = 51;
int led_direita = 52;

void velocidade(const std_msgs::Int16& vel){
  digitalWrite(motor_anda, HIGH);
  digitalWrite(motor_anda2, LOW);
  digitalWrite(pwm_anda, vel.data);  //Motor com rotação quase mediana (0-255)
  if(vel.data == 0){
    digitalWrite(motor_vira, HIGH);
    digitalWrite(motor_vira2, HIGH);
  }
//  if(vel.data == 0){
//    digitalWrite(motor_anda, HIGH);
//    digitalWrite(motor_anda2, HIGH);
//    digitalWrite(pwm_anda, 0);  //Motor com rotação quase mediana (0-255)
//    digitalWrite(motor_vira, HIGH);
//    digitalWrite(motor_vira2, HIGH);
//    
//    //LEDs
//    digitalWrite(led_vel_0, HIGH);
//    //digitalWrite(led_vel_50, LOW);
//    //digitalWrite(led_vel_100, LOW);
//    
//    Serial.println("PARA");
//  }
//  if(vel.data == 50){
//    digitalWrite(motor_anda, HIGH);
//    digitalWrite(motor_anda2, LOW);
//    digitalWrite(pwm_anda, 100);  //Motor com rotação quase mediana (0-255)
//
//    //LEDs
//    digitalWrite(led_vel_0, LOW);
//    //digitalWrite(led_vel_50, HIGH);
//    //digitalWrite(led_vel_100, LOW);
//    
//    Serial.println("BAIXA");
//}
//  if(vel.data == 100){
//    digitalWrite(motor_anda, HIGH);
//    digitalWrite(motor_anda2, LOW);
//    digitalWrite(pwm_anda, 255);  //Motor com rotação quase mediana (0-255)
//
//    //LEDs
//    digitalWrite(led_vel_0, LOW);
//    //digitalWrite(led_vel_50, LOW);
//    //digitalWrite(led_vel_100, HIGH);
//    //digitalWrite(led_ligado, HIGH);
//    
//    Serial.println("ALTA");
//  }
}

void direcao(const std_msgs::Int16& sentido){
  if(sentido.data == 0){
    digitalWrite(motor_vira, LOW);
    digitalWrite(motor_vira2, HIGH);

    //LEDs
    digitalWrite(led_esquerda, HIGH);
    //delay(500);
    digitalWrite(led_esquerda, LOW);
    //delay(500);
    //digitalWrite(led_frente, LOW);
    //digitalWrite(led_direita, LOW);
    
    Serial.println("ESQUERDA");
  }
  if(sentido.data == 1){
    digitalWrite(motor_vira, HIGH);
    digitalWrite(motor_vira2, HIGH);
    
    //LEDs
    //digitalWrite(led_esquerda, LOW);
    //digitalWrite(led_frente, HIGH);
    //digitalWrite(led_direita, LOW);
    
    Serial.println("FRENTE");
  }
  if(sentido.data == 2){
    digitalWrite(motor_vira, HIGH);
    digitalWrite(motor_vira2, LOW);

    //LEDs
    //digitalWrite(led_esquerda, LOW);
    //digitalWrite(led_frente, LOW);
    digitalWrite(led_direita, HIGH);
    //delay(500);
    digitalWrite(led_direita, LOW);
    //delay(500);
    
    Serial.println("DIREITA");
  }
}

ros::Subscriber<std_msgs::Int16> vel("car_vel_fuzzy", &velocidade);
ros::Subscriber<std_msgs::Int16> sentido("car_status_lane", &direcao);

void setup() {
  pinMode(motor_anda, OUTPUT);
  pinMode(motor_anda2, OUTPUT);
  pinMode(pwm_anda, OUTPUT);
  pinMode(motor_vira, OUTPUT);
  pinMode(motor_vira2, OUTPUT);
  pinMode(bt_lig, INPUT_PULLUP);
  pinMode(bt_desl, INPUT_PULLUP);
  pinMode(led_ligado, OUTPUT);
  pinMode(led_vel_0, OUTPUT);
  //pinMode(led_vel_50, OUTPUT);
  //pinMode(led_vel_100, OUTPUT);
  pinMode(led_esquerda, OUTPUT);
  //pinMode(led_frente, OUTPUT);
  pinMode(led_direita, OUTPUT);
  
  Serial.begin(9600);
  nh.initNode();
  nh.subscribe(vel);
  nh.subscribe(sentido);
}

void loop() {
  digitalWrite(motor_anda, HIGH);
  digitalWrite(motor_anda2, HIGH);
  digitalWrite(pwm_anda, 0);
  digitalWrite(motor_vira, HIGH);
  digitalWrite(motor_vira2, HIGH);
  if(digitalRead(bt_lig) == LOW){
    Serial.println("LIGADO");
    while(digitalRead(bt_desl) == HIGH){
      nh.spinOnce();
      Serial.println("PARA");
      delay(10);
      }
    }
    digitalWrite(led_ligado, LOW);
    digitalWrite(led_vel_0, HIGH);
    //digitalWrite(led_vel_50, LOW);
    //digitalWrite(led_vel_100, LOW);
    digitalWrite(led_esquerda, LOW);
    //digitalWrite(led_frente, LOW);
    digitalWrite(led_direita, LOW);
    digitalWrite(motor_anda, HIGH);
    digitalWrite(motor_anda2, HIGH);
    digitalWrite(pwm_anda, 0);
    digitalWrite(motor_vira, HIGH);
    digitalWrite(motor_vira2, HIGH);
}
