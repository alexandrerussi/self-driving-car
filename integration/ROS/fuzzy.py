#! C:/opt/ros/noetic/x64/python.exe
# coding: utf-8

import rospy
from std_msgs.msg import Int16

import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl

rospy.init_node("car_fuzzy")
pub = rospy.Publisher("/car_vel_fuzzy", Int16, queue_size=10)
rate = rospy.Rate(100)  # Hz

sensor_ultra = ctrl.Antecedent(np.arange(0, 2, 1), 'sensor_ultra')
velocidade = ctrl.Consequent(np.arange(0, 256, 1), 'velocidade')

sensor_ultra['baixo'] = fuzz.trimf(sensor_ultra.universe, [0, 0, 0.5])
sensor_ultra['alto'] = fuzz.trimf(sensor_ultra.universe, [0.5, 1, 1])

velocidade['baixo'] = fuzz.trimf(velocidade.universe, [0, 0, 0])
velocidade['médio'] = fuzz.trapmf(velocidade.universe, [0, 1, 254, 255])
velocidade['alto'] = fuzz.trimf(velocidade.universe, [254, 255, 255])

# yolo objeto
yolo_tipo_obj = ctrl.Antecedent(np.arange(0, 5, 1), 'yolo_tipo_obj')

yolo_tipo_obj['nada'] = fuzz.trimf(yolo_tipo_obj.universe, [0, 0, 1])
yolo_tipo_obj['remote'] = fuzz.trimf(yolo_tipo_obj.universe, [0, 1, 2])
yolo_tipo_obj['copo'] = fuzz.trimf(yolo_tipo_obj.universe, [1, 2, 3])
yolo_tipo_obj['pare'] = fuzz.trimf(yolo_tipo_obj.universe, [2, 3, 4])
yolo_tipo_obj['pessoa'] = fuzz.trimf(yolo_tipo_obj.universe, [3, 4, 4])


class FuzzySubscriber:
    def __init__(self):
        self.ultrasonic = None
        self.object = None
        self.status_botao = None

    def ultrasonic_callback(self, msg):
        # "Store" message received.
        self.ultrasonic = msg.data

        # Compute stuff.
        self.main_fuzzy()

    def object_callback(self, msg):
        # "Store" the message received.
        self.object = msg.data

        # Compute stuff.
        self.main_fuzzy()

    def botao_callback(self, msg):
        # "Store" the message received.
        self.status_botao = msg.data

    def main_fuzzy(self):
        rospy.loginfo(self.object)
        # rospy.loginfo("Ultra", self.ultrasonic)

        # correct rules
        # self.status_botao = 2

        velocidade_ctrl = None
        if self.status_botao == 0:
            vel_fuzzy = Int16()
            vel_fuzzy.data = 0
            pub.publish(vel_fuzzy)
            rate.sleep()
        else:
            if self.status_botao == 1:
                rule1 = ctrl.Rule(sensor_ultra['baixo'] & yolo_tipo_obj['nada'], velocidade['alto'])
                rule2 = ctrl.Rule(sensor_ultra['baixo'] & yolo_tipo_obj['pessoa'], velocidade['médio'])
                rule3 = ctrl.Rule(sensor_ultra['alto'] & yolo_tipo_obj['nada'], velocidade['baixo'])
                rule4 = ctrl.Rule(sensor_ultra['alto'] & yolo_tipo_obj['pessoa'], velocidade['baixo'])
                rule5 = ctrl.Rule(yolo_tipo_obj['pare'], velocidade['baixo'])
                rule6 = ctrl.Rule(yolo_tipo_obj['remote'], velocidade['alto'])
                rule7 = ctrl.Rule(yolo_tipo_obj['copo'], velocidade['alto'])
                velocidade_ctrl = ctrl.ControlSystem([rule1, rule2, rule3, rule4, rule5, rule6, rule7])
            elif self.status_botao == 2:
                rule1 = ctrl.Rule(sensor_ultra['baixo'] & yolo_tipo_obj['nada'], velocidade['alto'])
                rule2 = ctrl.Rule(sensor_ultra['baixo'] & yolo_tipo_obj['pessoa'], velocidade['médio'])
                rule3 = ctrl.Rule(sensor_ultra['alto'] & yolo_tipo_obj['nada'], velocidade['baixo'])
                rule4 = ctrl.Rule(sensor_ultra['alto'] & yolo_tipo_obj['pessoa'], velocidade['baixo'])
                rule5 = ctrl.Rule(yolo_tipo_obj['pare'], velocidade['baixo'])
                rule6 = ctrl.Rule(yolo_tipo_obj['remote'], velocidade['baixo'])
                rule7 = ctrl.Rule(yolo_tipo_obj['copo'], velocidade['alto'])
                velocidade_ctrl = ctrl.ControlSystem([rule1, rule2, rule3, rule4, rule5, rule6, rule7])
            elif self.status_botao == 3:
                rule1 = ctrl.Rule(sensor_ultra['baixo'] & yolo_tipo_obj['nada'], velocidade['alto'])
                rule2 = ctrl.Rule(sensor_ultra['baixo'] & yolo_tipo_obj['pessoa'], velocidade['médio'])
                rule3 = ctrl.Rule(sensor_ultra['alto'] & yolo_tipo_obj['nada'], velocidade['baixo'])
                rule4 = ctrl.Rule(sensor_ultra['alto'] & yolo_tipo_obj['pessoa'], velocidade['baixo'])
                rule5 = ctrl.Rule(yolo_tipo_obj['pare'], velocidade['baixo'])
                rule6 = ctrl.Rule(yolo_tipo_obj['copo'], velocidade['baixo'])
                rule7 = ctrl.Rule(yolo_tipo_obj['remote'], velocidade['alto'])
                velocidade_ctrl = ctrl.ControlSystem([rule1, rule2, rule3, rule4, rule5, rule6, rule7])

            velocidade_simulador = ctrl.ControlSystemSimulation(velocidade_ctrl)

            velocidade_simulador.input['sensor_ultra'] = self.ultrasonic
            velocidade_simulador.input['yolo_tipo_obj'] = self.object

            # Computando o resultado
            velocidade_simulador.compute()

            vel_fuzzy = Int16()
            vel_fuzzy.data = int(round(velocidade_simulador.output['velocidade']))
            pub.publish(vel_fuzzy)
            rate.sleep()


if __name__ == '__main__':
    fuzzy_sub = FuzzySubscriber()

    rospy.Subscriber('/car_status_ultrasonic', Int16, fuzzy_sub.ultrasonic_callback)
    rospy.Subscriber('/car_status_object', Int16, fuzzy_sub.object_callback)

    rospy.Subscriber('/car_status_botao', Int16, fuzzy_sub.botao_callback)

    rospy.spin()
