#! C:/opt/ros/noetic/x64/python.exe
# coding: utf-8

import rospy
from std_msgs.msg import Int16

import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import os

# Fetch the service account key JSON file contents
cur_dir = os.getcwd()
cred = credentials.Certificate(cur_dir + '\\deloreanapp-firebase.json')

# Initialize the app with a service account, granting admin privileges
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://deloreanapp.firebaseio.com/'
})
status_lane_db = db.reference('/Passivo/car_status_lane')

rospy.init_node("subscriber_lane")

conta = 0


def lane_callback(msg):
    # "Store" message received.
    global conta
    status_lane = msg.data
    if status_lane == 0 and conta != 0:
        status_lane_db.set("0")
        conta = 0
    elif status_lane == 1 and conta != 1:
        status_lane_db.set("1")
        conta = 1
    elif status_lane == 2 and conta != 2:
        status_lane_db.set("2")
        conta = 2


rospy.Subscriber('/car_status_lane', Int16, lane_callback)

rospy.spin()
