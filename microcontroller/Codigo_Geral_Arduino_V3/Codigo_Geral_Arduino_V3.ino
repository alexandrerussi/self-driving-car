#include "Ultrasonic.h"

#include <ros.h>
#include <std_msgs/Int16.h>

ros::NodeHandle  nh;

std_msgs::Int16 str_msg;
ros::Publisher car_status_ultrasonic("car_status_ultrasonic", &str_msg);

int S1;
int S2;
int S3;
int S4;
int saida;

HC_SR04 ultra_frente_direita(37,36); //Configura os pinos sensor ultrassonico (Trigger,Echo)
HC_SR04 ultra_direita(39,38); //Configura os pinos sensor ultrassonico (Trigger,Echo)
HC_SR04 ultra_frente_esquerda(33,32); //Configura os pinos sensor ultrassonico (Trigger,Echo)
HC_SR04 ultra_esquerda(35,34); //Configura os pinos sensor ultrassonico (Trigger,Echo)

void setup() {
  nh.initNode();
  nh.advertise(car_status_ultrasonic);
}

void loop() {
  if ( 70 > ultra_frente_direita.distance()){
  S1 = 1;  
  } else{
  S1 = 0;
  }

//  if ( 70 > ultra_direita.distance()){
//  S2 = 1;
//  } else{
//  S2 = 0;
//  }

  if ( 70 > ultra_frente_esquerda.distance()){
  S3 = 1;
  } else{
  S3 = 0;
  }

  if ( 70 > ultra_esquerda.distance()){
  S4 = 1;
  } else{
  S4 = 0;
  }

  if (S1 == 1 or S2 == 1 or S3 == 1 or S4 == 1){
    saida = 1;
  } else {
    saida = 0;
  }

  str_msg.data = saida;
  car_status_ultrasonic.publish( &str_msg );
  nh.spinOnce();
  delay(10);
}
